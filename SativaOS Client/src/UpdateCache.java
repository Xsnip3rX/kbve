import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.swing.JOptionPane;
import sign.signlink;

public class UpdateCache implements Runnable {

	public static final String ZIP_URL = "https://dl.dropboxusercontent.com/s/qma3p71omhvl8x6/SativaOSV1.zip?dl=1"; // cache link
	public static final String VERSION_URL = "http://rspsdesigns.net/cacheVersion.txt";
	public static final String VERSION_FILE = signlink.findcachedir()+"cacheVersion.dat";
	private client client;
	client frame;

	public UpdateCache(client client) {
		this.client = client;
	}

	private void drawLoadingText(int amount, String text) {
		client.drawLoadingText(amount, text);
	}
	
	@SuppressWarnings("resource")
	public double getCurrentVersion(){
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(VERSION_FILE)));
			return Double.parseDouble(br.readLine());
		} catch (Exception e) {
			return 0.1;
		}
	}
	
	public double getNewestVersion(){
		try {
			URL tmp = new URL(VERSION_URL);
			BufferedReader br = new BufferedReader(new InputStreamReader(tmp.openStream()));
			return Double.parseDouble(br.readLine());
		} catch (Exception e) {
			handleException(e);
			return -1;
		}
	}
	
	@SuppressWarnings("unused")
	private void handleException(Exception e){
		StringBuilder strBuff = new StringBuilder();
		strBuff.append("Please Screenshot this message, and send it to an admin!\r\n\r\n");
        StringBuilder append = strBuff.append(e.getClass().getName()).append(" \"").append(e.getMessage()).append("\"\r\n");
		for(StackTraceElement s : e.getStackTrace())
			strBuff.append(s.toString()).append("\r\n");
		alert("Exception [" + e.getClass().getSimpleName() + "]",strBuff.toString(),true);
	}
	
	private void alert(String msg){
		alert("Message",msg,false);
	}
	
	private void alert(String title,String msg,boolean error){
		JOptionPane.showMessageDialog(null,
			   msg,
			   title,
			    (error ? JOptionPane.ERROR_MESSAGE : JOptionPane.PLAIN_MESSAGE));
	}
	
	@SuppressWarnings("resource")
	@Override
	public void run() {
	drawLoadingText(0, "Checking Cache Versions");
		try{
		double newest = getNewestVersion();
		if(newest > this.getCurrentVersion()){
			int n = JOptionPane.showConfirmDialog(
				    null,
				    "There is an update to version " + newest + "\n" +
					"Would you like to update?",
				    "Current version: "+ getCurrentVersion(),
				    JOptionPane.YES_NO_OPTION);
			if(n == 0){
				updateclient();
				drawLoadingText(0, "Cache has been updated. Please reload!");
				alert("The cache has been updated, please restart the client or reload your browser!");
				OutputStream out = new FileOutputStream(VERSION_FILE);
				out.write(String.valueOf(newest).getBytes());;
				System.exit(0);
			}else{
				alert(" Please update your client! V" + getCurrentVersion());
				System.exit(0);
			}
		}
		}catch(Exception e){
			handleException(e);
		}
	}
	
	private void updateclient() {
		File clientZip = downloadclient();
		if(clientZip != null){
			unZip(clientZip);
		}
	}
	
	private void unZip(File clientZip) {
		try {
			unZipFile(clientZip,new File(signlink.findcachedir()));
			clientZip.delete();
		} catch (IOException e) {
			handleException(e);
		}
	}
	public final int BUFFER = 1024;
	@SuppressWarnings("resource")
	public void unZipFile(File zipFile,File outFile) throws IOException{
		ZipInputStream zin = new ZipInputStream(new BufferedInputStream(new FileInputStream(zipFile)));
		ZipEntry e;
		long max = 0;
		long curr = 0;
		while((e = zin.getNextEntry()) != null)
			max += e.getSize();
		zin.close();
		ZipInputStream in = new ZipInputStream(new BufferedInputStream(new FileInputStream(zipFile)));
		while((e = in.getNextEntry()) != null){
			if(e.isDirectory())
				new File(outFile,e.getName()).mkdirs();
			else{
				//OutputStream out = new java.io.FileOutputStream(new File(outFile,e.getName()));
				FileOutputStream out = new FileOutputStream(new File(outFile,e.getName()));
				byte[] b = new byte[BUFFER];
				int len = 0;
				setUnzipPercent((int)((curr * 100) / max));
				while((len = in.read(b,0,b.length)) > -1){
						curr += len;
						out.write(b, 0, len);
						//setUnzipPercent((int)((curr * 100) / max));
				}
				out.flush();
				out.close();
				/*FileOutputStream out = new FileOutputStream(new File(outFile,e.getName()));
				byte [] b = new byte[BUFFER];
				int len = 0;
				while ((len = in.read(b)) != -1) {
					out.write(b,0,len);
				}
				out.close();*/
			}
		}
	}

	public int percent = 0;
	
	public void setDownloadPercent(int amount){
	        percent = amount;
			drawLoadingText(amount, "SativaOS: Downloading Cache" + " - ");
	}
	
	public int percent2 = 0;
	
	public void setUnzipPercent(int amount2){
	        percent2 = amount2;
			drawLoadingText(amount2, "SativaOS: Extracting Cache" + " - ");
	}

	private File downloadclient(){
		File ret = new File(signlink.findcachedir()+"SativaOSV1.zip"); // let me upload cacheVersion.txt now.
		try{
		OutputStream out = new FileOutputStream(ret);
		URLConnection conn = new URL(ZIP_URL).openConnection();
		InputStream in = conn.getInputStream();
		long max = conn.getContentLength();
		long curr = 0;
		byte[] b = new byte[1024];
		int len;
		while((len = in.read(b, 0, b.length)) > -1){
			out.write(b,0,len);
			curr += len;
			setDownloadPercent((int)((curr * 100) / max));
		}
		out.flush();
		out.close();
		in.close();
		return ret;
		}catch(Exception e){
			handleException(e);
				ret.delete();
			return null;
		}
	}
}