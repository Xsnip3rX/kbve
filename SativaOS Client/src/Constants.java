public interface Constants {
	public double CLIENT_VERSION = 2.5;
	public String CLIENT_VERSION_URL = "http://rspsdesigns.net/version.txt"; // upload your version file, if you have one
	public String CLIENT_DOWNLOAD_URL = ""; // When you get new client uploaded to your website, place here.
	public String CACHE_NAME = "SativaOSV1"; // Cache name?
	public String DESKTOP_PATH =
			System.getProperty("user.home") +
			System.getProperty("file.separator") +
			"Desktop" +
			System.getProperty("file.separator");
	public String LOCALHOST = "192.168.1.105";
	public String SERVER_INTERNET_PROTOCOL = "127.0.0.1"; // Websites IP
	public String SERVER = LOCALHOST;
	//public String SERVER = SERVER_INTERNET_PROTOCOL;
}

