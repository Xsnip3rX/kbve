import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


/**
 * 
 * @author Jason http://www.rune-server.org/members/andrewwtm
 * @date Mar 12, 2014
 */
public class Update {
	client client;
	boolean active;
	double version;
	
	public Update(client client) {
		this.client = client;
		this.active = true;
	}
	
	public void run() {
		while(active) {
			try {
				setText(0, "Checking client version");
				BufferedReader reader = new BufferedReader(new InputStreamReader(new URL(Constants.CLIENT_VERSION_URL).openStream()));
				String line = null;
				while((line = reader.readLine()) != null) {
					if(line.startsWith("//"))
						continue;
					String[] tokens = line.split("=");
					String key = tokens[0];
					String value = tokens[1];
					if(key.equals("state")) {
						if(!value.equalsIgnoreCase("active")
								&& !value.equalsIgnoreCase("inactive")
									|| value.equalsIgnoreCase("inactive")) {
							active = false;
							break;
						}
					} else if(key.equals("version")) {
						try { 
							version = Double.parseDouble(value);
						} catch (NumberFormatException nfe) {
							setText(0, "Error connecting to host");
							active = false;
							break;
						}
					}
				}
				reader.close();
				if(version != Constants.CLIENT_VERSION) {
					download();
				}
				active = false;
			} catch (Exception e) {
				setText(0, "Fatal error updating client");
			}
		}
	}
	
	void setText(int percent, String text) {
		client.drawLoadingText(percent, text);
	}
	
	void download() {
		try {
			final String path = Constants.DESKTOP_PATH+"SativaOS "+version+".jar";
			URL url = new URL(Constants.CLIENT_DOWNLOAD_URL);
			HttpURLConnection http = (HttpURLConnection) url.openConnection();
			long fileSize = http.getContentLengthLong();
			BufferedInputStream bufferedInputStream = new BufferedInputStream(new URL(Constants.CLIENT_DOWNLOAD_URL).openStream());
			FileOutputStream fileOuputStream = new FileOutputStream(path);
			BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOuputStream);
			int read;
			int total = 0;
			byte[] data = new byte[1024];
			while((read = bufferedInputStream.read(data)) != -1) {
				bufferedOutputStream.write(data, 0, read);
				total += read;
				if(fileSize <= 0 || total <= 0) {
					setText(100, "Downloading Client");
					continue;
				}
				int percent = (int)(total / (fileSize / 100));
				if(percent < 0)
					percent = 0;
				setText(percent, "Downloading Client");
			}
			bufferedInputStream.close();
			bufferedOutputStream.close();
			Runtime.getRuntime().exec(new String[] {"java", "-jar", path});
			Runtime.getRuntime().exit(0);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
