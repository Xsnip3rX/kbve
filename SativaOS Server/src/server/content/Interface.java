package server.content;

import server.game.players.Client;

/**
 * Interface ID's for checking.
 * @author Indrek
 *
 */
public class Interface {
	
	public static int BANK = 5063;
	public static int BANK_PIN = 7424;
	public static int DUEL_1 = 6575;
	public static int DUEL_2 = 6412;
	public static int TRADE_1 = 3323;
	public static int TRADE_2 = 3443;
	public static int SHOP = 3824;
	public static int DEPOSIT_BOX = -1;
	public static final int SKILL_INTERFACE = 8714;
	
	private Client c;
	public Interface(Client c) {
		this.c = c;
	}
	
	public boolean openInterface(int id) {
		if(c.openInterfaceId == id) {
			return true;
		} else {
			c.sendMessage("Invalid interface.");
			return false;
		}
	}

}
