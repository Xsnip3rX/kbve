package server.game.players;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;

import core.util.log.Logger;
import server.game.items.ItemAssistant;

/**
 * ActionLogger class - Used for logging players actions.
 * @author Indrek
 *
 */
public class ActionLogger {
	
	private Client c;
	public ActionLogger(Client c) {
		this.c = c;
	}
	
	/**
	 * Logger with specified filename.
	 * @param playerName - The players name.
	 * @param action - The action the player performed.
	 * @param fileName - The files name.
	 */
	public static void log(String playerName, String action, String fileName) {
		try {
			BufferedWriter bItem = new BufferedWriter(new FileWriter("./logs/" + fileName + ".txt", true));
			@SuppressWarnings("resource")
			Logger log = new Logger(System.out);
			try {
				bItem.newLine();
				bItem.write("[" + log.getPrefix() + "]: Player: " + playerName + " | Action: " + action);
				bItem.newLine();
				bItem.write("--------------------------------------------------");
				System.setOut(new Logger(System.out));
			} finally {
				bItem.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Logger with specified file name and folder.
	 * @param playerName - The players name.
	 * @param action - The action the player performed.
	 * @param folder - The folder where the files are stored.
	 * @param fileName - The files name.
	 */
	public static void log(String playerName, String action, String folder, String fileName) {
		//BufferedWriter bItem = null;
		try {
			BufferedWriter bItem = new BufferedWriter(new FileWriter("./logs/"+folder+"/" + fileName + ".txt", true));
			//bItem = new BufferedWriter(new FileWriter("./logs/"+folder+"/"+ playerName+ ".txt"));
			@SuppressWarnings("resource")
			Logger log = new Logger(System.out);
			try {
				bItem.newLine();
				bItem.write("[" + log.getPrefix() + "]: Player: " + playerName + " | Action: " + action);
				bItem.newLine();
				bItem.write("--------------------------------------------------");
				System.setOut(new Logger(System.out));
			} finally {
				bItem.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	* Will write what kind of item a player has received.
	* MONTH = 0 = January
	* DAY OF MONTH = 30 || 31
	*/
	public void tradeReceived(String itemName, int itemAmount) {
		Client o = (Client) PlayerHandler.players[c.tradeWith];
		Calendar C = Calendar.getInstance();
		try {
			BufferedWriter bItem = new BufferedWriter(new FileWriter("./logs/trades/received/" + c.playerName + ".txt", true));
			try {		
				bItem.newLine();
				bItem.write("Year : " + C.get(Calendar.YEAR) + "\tMonth : " + C.get(Calendar.MONTH) + "\tDay : " + C.get(Calendar.DAY_OF_MONTH));
				bItem.newLine();
				bItem.write("Received " + itemAmount + " " + itemName + " From " + o.playerName);
				bItem.newLine();
				bItem.write("--------------------------------------------------");
			} finally {
				bItem.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	* Will write what kind of item a player has traded with another player.
	* MONTH = 0 = January
	* DAY OF MONTH = 30 || 31
	*/
	public void tradeGive(String itemName, int itemAmount) {
		Client o = (Client) PlayerHandler.players[c.tradeWith];
		Calendar C = Calendar.getInstance();
		try {
			BufferedWriter bItem = new BufferedWriter(new FileWriter("./logs/trades/gave/" + c.playerName + ".txt", true));
			try {
				bItem.newLine();
				bItem.write("Year : " + C.get(Calendar.YEAR) + "\tMonth : " + C.get(Calendar.MONTH) + "\tDay : " + C.get(Calendar.DAY_OF_MONTH));
				bItem.newLine();
				bItem.write("Gave " + itemAmount + " " + itemName + " To " + o.playerName);
				bItem.newLine();
				bItem.write("--------------------------------------------------");
			} finally {
				bItem.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void stakeLog(String playerName) {
		try {
			BufferedWriter bItem = new BufferedWriter(new FileWriter("./logs/stakes/" + playerName + ".txt", true));
			Client o1 = (Client) PlayerHandler.players[c.duelingWith];
			@SuppressWarnings("resource")
			Logger log = new Logger(System.out);
			try {
				bItem.newLine();
				bItem.write("[Date: "+log.getPrefix()+"]");
				bItem.newLine();
				bItem.write("[Staked with: "+o1.playerName+"]");
				for(int i = 0; i<c.getDueling().stakedItems.size(); i++) {
					bItem.newLine();
					bItem.write(c.playerName+" Staked: "+ItemAssistant.getItemName(c.getDueling().stakedItems.get(i).id)+" Amount: "+c.getDueling().stakedItems.get(i).amount);
					
				}
				for(int i = 0; i<o1.getDueling().stakedItems.size(); i++) {
					bItem.newLine();
					bItem.write(o1.playerName+" Staked: "+ItemAssistant.getItemName(o1.getDueling().stakedItems.get(i).id)+" Amount: "+o1.getDueling().stakedItems.get(i).amount);
				}
				bItem.newLine();
				bItem.write("--------------------------------------------------");
				System.setOut(new Logger(System.out));
			} finally {
				bItem.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
