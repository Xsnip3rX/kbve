package server.game.players;

import server.game.npcs.NPC;

/**
 * 
 * @author Jason http://www.rune-server.org/members/jason
 * @date Mar 2, 2014
 */
public class Boundary {
	
	int minX, minY, highX, highY;
	
	/**
	 * 
	 * @param minX The south-west x coordinate
	 * @param minY The south-west y coordinate
	 * @param highX The north-east x coordinate
	 * @param highY The north-east y coordinate
	 */
	public Boundary(int minX, int minY, int highX, int highY) {
		this.minX = minX;
		this.minY = minY;
		this.highX = highX;
		this.highY = highY;
	}
	
	/**
	 * 
	 * @param player The player object
	 * @param boundaries The array of Boundary objects
	 * @return
	 */
	public static boolean isIn(Player player, Boundary[] boundaries) {
		for(Boundary b : boundaries)
			if (player.absX >= b.minX && player.absX <= b.highX && player.absY >= b.minY && player.absY <= b.highY)
				 return true;
		return false;
	}
	
	/**
	 * 
	 * @param player The player object
	 * @param boundaries The boundary object
	 * @return
	 */
	public static boolean isIn(Player player, Boundary boundaries) {
		return player.absX >= boundaries.minX && player.absX <= boundaries.highX 
				&& player.absY >= boundaries.minY && player.absY <= boundaries.highY;
	}
	
	/**
	 * 
	 * @param npc The npc object
	 * @param boundaries The boundary object
	 * @return
	 */
	public static boolean isIn(NPC npc, Boundary boundaries) {
		return npc.absX >= boundaries.minX && npc.absX <= boundaries.highX 
				&& npc.absY >= boundaries.minY && npc.absY <= boundaries.highY;
	}
	
	public static int entitiesInArea(Boundary boundary) {
		int i = 0;
		for(Player player : PlayerHandler.players)
			if(player != null)
				if(isIn(player, boundary))
					i++;
		return i;
	}
	
	public static final Boundary PEST_CONTROL_AREA = new Boundary(2650, 2635, 2675, 2655);
	public static final Boundary FIGHT_CAVE = new Boundary(2365, 5055, 2429, 5122);
}
