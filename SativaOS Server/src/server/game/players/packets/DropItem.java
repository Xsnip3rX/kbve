package server.game.players.packets;

import server.Config;
import server.Server;
import server.event.CycleEventHandler;
import server.game.minigames.castlewars.CastleWars;
import server.game.players.Client;
import server.game.players.PacketType;
import server.game.players.PlayerSave;
import server.content.skills.*;

/**
 * Drop Item
 **/
public class DropItem implements PacketType {

	@SuppressWarnings("unused")
	@Override
	public void processPacket(Client c, int packetType, int packetSize) {
		int itemId = c.getInStream().readUnsignedWordA();
		c.getInStream().readUnsignedByte();
		c.getInStream().readUnsignedByte();
		int slot = c.getInStream().readUnsignedWordA();
		c.alchDelay = System.currentTimeMillis();
		CycleEventHandler.getSingleton();
		CycleEventHandler.stopEvents(c);
		if(c.playerSkilling[10])
			Fishing.resetFishing(c);
		if(!c.getItems().playerHasItem(itemId)) {
			return;
		}
		if(c.inTrade || c.inDuel || c.isBanking) {
			c.sendMessage("Please close the interface you have open first!");
			return;
		}
		if(c.isDead) {
			return;
		}
		if(c.inWild() && c.getShops().getItemShopValue(itemId) > 25000) {
			c.sendMessage("You can't drop this item inside the wilderness!");
			return;
		}
		if(c.inDuelArena()) {
			c.sendMessage("You can't drop items inside the arena!");
			return;
		}
		
		if(itemId == 4045) {
			int explosiveHit = 15;
			c.startAnimation(827);
			c.getItems().deleteItem(itemId, slot, c.playerItemsN[slot]);
			c.handleHitMask(explosiveHit);
			c.dealDamage(explosiveHit);
			c.getPA().refreshSkill(3);
			c.forcedText = "Ow! That really hurt!";
			c.forcedChatUpdateRequired = true;
			c.updateRequired = true;
		}

		boolean droppable = true;
		for (int i : Config.UNDROPPABLE_ITEMS) {
			if (i == itemId) {
				droppable = false;
				break;
			}
		}
		if (c.playerItemsN[slot] != 0 && itemId != -1
				&& c.playerItems[slot] == itemId + 1) {
			if (droppable) {
				if (c.underAttackBy > 0 || c.underAttackBy2 > 0) {
					if (c.getShops().getItemShopValue(itemId) > 1000) {
						c.sendMessage("You may not drop items worth more than 1000 while in combat.");
						return;
					}
				}
				// Server.itemHandler.createGroundItem(c, itemId, c.getX(),
				// c.getY(), c.playerItemsN[slot], c.getId());
			}
			boolean isBone = false;
			switch (itemId) {
			case 4045:
				if (CastleWars.isInCw(c)) {
					int explosiveHit = 15;
					c.startAnimation(827);
					c.getItems().deleteItem(itemId, slot, c.playerItemsN[slot]);
					c.handleHitMask(explosiveHit);
					c.dealDamage(explosiveHit);
					c.getPlayerAssistant().refreshSkill(3);
					c.forcedText = "Ow! That really hurt!";
					c.forcedChatUpdateRequired = true;
					c.updateRequired = true;
				} else {
					c.getItems().deleteItem(4045, c.getItems().getItemAmount(4045));
					c.getItems().deleteItem(4046, c.getItems().getItemAmount(4046));
					c.sendMessage("You can't do that! Your not in castle wars!");
				}
				break;
			case 590:
				if(c.absX == 2705 && c.absY == 3497) {
					c.startAnimation(886);
				}
				break;
			case 526:
			case 528:
			case 530:
			case 532:
			case 534:
			case 536:
			case 538:
			case 540:
			case 7409:
			case 9044:
			case 775:
			case 776:
			case 10771:
				isBone = true;
				break;
			}
			if (c.playerRights == 3) {
				Server.itemHandler.createGroundItem(c, itemId, c.getX(), c.getY(), c.playerItemsN[slot], c.getId());
				c.getItems().deleteItem(itemId, slot, c.playerItemsN[slot]);					
				PlayerSave.saveGame(c);
			} else {
				if (c.getShops().getItemShopValue(itemId) < 1000 && itemId != 995 && !isBone) {
					c.sendMessage("Your item disappears as it touches the ground.");
					c.getItems().deleteItem(itemId, slot, c.playerItemsN[slot]);
					PlayerSave.saveGame(c);
				} else if (c.getShops().getItemShopValue(itemId) >= 1000
						|| itemId == 995 || isBone) {
					boolean destroy = true;
					c.droppedItem = itemId;
					c.getPA().destroyInterface(itemId);
					PlayerSave.saveGame(c);
				} else {
					c.sendMessage("This items cannot be dropped.");
				}
			}
		}

	}
}
